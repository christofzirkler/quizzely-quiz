import UIKit
import SwiftyUserDefaults

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let appRootNavigationController = UINavigationController()
        let appCoordinator = AppCoordinator(navigationController: appRootNavigationController)
        self.window!.rootViewController = appRootNavigationController
        self.window!.makeKeyAndVisible()
        appCoordinator.start()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
}

extension DefaultsKeys {
    static let highscore = DefaultsKey<Int>("pp_highscore")
    static let overallscore = DefaultsKey<Int>("pp_overall_score")
}

