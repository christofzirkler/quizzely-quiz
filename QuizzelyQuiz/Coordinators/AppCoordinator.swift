import Foundation
import UIKit

class AppCoordinator {

    let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        // Usually in more complex apps you want now check weather the user is logged and decide
        // based on that if you want to show the actual content screen or a "login / signup" screen.
        // We simply show the main menu here.

        let mainMenuVC = MainMenuVC()
        mainMenuVC.coordinator = self
        self.navigationController.pushViewController(mainMenuVC, animated: true)
    }
}

extension AppCoordinator: MainMenuVCDelegate {

    func didClickBttNewGame() {
        let playCoordinator = PlayCoordinator(navigationController: self.navigationController)
        playCoordinator.start()
    }
}
