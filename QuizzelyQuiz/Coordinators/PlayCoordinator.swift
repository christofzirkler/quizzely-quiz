import Foundation
import UIKit
import  RxSwift

class PlayCoordinator {

    let navigationController: UINavigationController
    var questions: [QuizQuestion]?
    let hostingNavigationController: UINavigationController
    var questionIndex = 0
    var currentScore = 0
    var remainingSeconds = 30
    var timer: Timer?

    // A "DisposeBag" is the swift way of managing memory when using Rx subscription.
    // Once the diposeBag is de-referenced, all the subscriptions in the bag will be de-referenced.
    let disposeBag = DisposeBag()

    let playVC = PlayVC()

    init(navigationController: UINavigationController) {
        self.hostingNavigationController = navigationController
        self.navigationController = UINavigationController(rootViewController: self.playVC)
        playVC.coordinator = self
    }

    func start() {
        // Load the questions for this round
        QuizQuestionsRepository
            .get30RandomQuizQuestion()
            .observeOn(MainScheduler.instance)
            .subscribe(
                onNext: { quizQuestions in
                    self.questions = quizQuestions
                    self.showFirstQuestions()
                },
                onError: { error in
                    // TODO: Error handling :)
                    print("error")
                }
            )
            .disposed(by: self.disposeBag)
    }

    func showFirstQuestions() {
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(secondEllapsed), userInfo: nil, repeats: true)
        self.playVC.setupWithQuestion(
            question: self.questions!.first!,
            questionIndex: questionIndex,
            maxQuestions: self.questions!.count)

        self.playVC.setScore(score: 0)
        self.hostingNavigationController.present(navigationController, animated: true, completion: nil)
    }

    @objc func secondEllapsed() {
        self.remainingSeconds = remainingSeconds - 1
        self.playVC.setRemainingTime(seconds: self.remainingSeconds)

        if self.remainingSeconds <= 0 {
            // Timeout, 30s passed.
            self.timer?.invalidate()
            let answerFailedVC = AnswerFailedVC()
            answerFailedVC.setReason(reason: .timeout)
            answerFailedVC.coordinator = self
            self.navigationController.pushAndClearViewController(viewController: answerFailedVC)
        }
    }

    func calculateScoreEarnedForRound(roundIndex: Int) -> Int {
        // Calculate the earned scores
        // Score earned in this round is calculated by 2^questionIndex
        // 2^0 = 1, 2^1 = 2, 2^2 = 4, 2^3 = 8, ...
        // pow only works on Doubles in Swift per default, so we need some casting here
        let scoreEarnedThisRound = Int(pow(2.0, Double(roundIndex)))
        return scoreEarnedThisRound
    }
}

extension PlayCoordinator: PlayVCDelegate {

    func didClickCheck(userAnswer: String) {

        self.timer?.invalidate()

        // Check if the supplied answer is correct
        let currentQuestion = self.questions![questionIndex]
        if userAnswer.lowercased() == currentQuestion.answer.lowercased() {

            self.currentScore = self.currentScore + calculateScoreEarnedForRound(roundIndex: self.questionIndex)

            // Answer was correct, no check if we are done with all question or if we need to show the next question.
            if self.questionIndex == self.questions!.count - 1 {
                // We are done, show the winning screen
                let gameWonVC = GameWonVC()
                gameWonVC.coordinator = self
                self.navigationController.pushAndClearViewController(viewController: gameWonVC)
            } else {
                // Send user to the AnswerCorrectVC
                let answerCorrectVC = AnswerCorrectVC()
                answerCorrectVC.coordinator =  self
                self.navigationController.pushAndClearViewController(viewController: answerCorrectVC)
            }
        }  else {
            // Answer was incorrect
            let answerFailedVC = AnswerFailedVC()
            answerFailedVC.setReason(reason: .incorrectAnswer)
            answerFailedVC.coordinator = self
            self.navigationController.pushAndClearViewController(viewController: answerFailedVC)
        }
    }

    func didClickHint() {
        // For now, we simply fill the answer text box with the correct solution.
        let hint = questions![questionIndex].answer
        self.playVC.showHint(hint: hint)
    }
}

extension PlayCoordinator: AnswerCorrectDelegate  {
    func didClickNextQuestion() {
        self.questionIndex = self.questionIndex + 1
        self.playVC.setupWithQuestion(
            question: self.questions![self.questionIndex],
            questionIndex: self.questionIndex,
            maxQuestions: self.questions!.count)
        self.playVC.setScore(score: self.currentScore)
        self.remainingSeconds = 30
        self.playVC.setRemainingTime(seconds: self.remainingSeconds)
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(secondEllapsed), userInfo: nil, repeats: true)
        self.navigationController.pushAndClearViewController(viewController: self.playVC)
    }
}

extension PlayCoordinator: AnswerFailedVCDelegate {
    func didClickBttGoToMainMenu() {
        self.navigationController.dismiss(animated: true, completion: nil)

        // Update the scores
        ScoreDAO.addToOverallScore(score: self.currentScore)
        if currentScore > ScoreDAO.getHighscore() {
            ScoreDAO.setHighscore(newHighscore: self.currentScore)
        }
    }
}

extension PlayCoordinator: GameWonVCDelegate {
    // didClickBttGoToMainMenu already implemented in AnswerFailedVCDelegate
}

