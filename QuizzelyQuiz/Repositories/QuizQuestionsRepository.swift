import Foundation
import RxSwift

/**
 * The QuizQuestionsRepository loads Quiz questions from an external data
 * source and converts that data so it can be used in our app.
 */
class QuizQuestionsRepository {

    static let API_URL = "http://jservice.io/api/random?count=30"

    class func get30RandomQuizQuestion() -> Observable<[QuizQuestion]> {
        return Observable.create({ observer -> Disposable in
            URLSession.shared.dataTask(
                with: URL(string: API_URL)!,
                completionHandler: { (data: Data?, urlResponse: URLResponse?, error: Error?) in
                    if let error = error {
                        observer.onError(error)
                    }
                    guard let responseData = data else {
                        observer.onError(NSError())
                        return
                    }
                    print(responseData)

                    do {
                        let quizQuestions = try JSONDecoder().decode([QuizQuestion].self, from: responseData)
                        observer.onNext(quizQuestions)
                    } catch {
                        observer.onError(error)
                    }
                }
            ).resume()
            return Disposables.create()
        })
    }
}
