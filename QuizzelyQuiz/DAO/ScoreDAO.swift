import Foundation
import SwiftyUserDefaults

/**
 * The ScoreDAO (Data Access Object) takes care of reading and persisting scores locally.
 */
class ScoreDAO {
    class func getHighscore() -> Int {
        return Defaults[.highscore]
    }

    class func getOverallScore() -> Int {
        return Defaults[.overallscore]
    }

    class func setHighscore(newHighscore: Int) {
        Defaults[.highscore] = newHighscore
    }

    class func addToOverallScore(score: Int) {
        Defaults[.overallscore] += score
    }
}
