import Foundation
import UIKit

class AnswerFailedVCView: UIView {

    lazy var lblTitle: UILabel = {
        let  label = UILabel()
        label.text = ""
        label.textAlignment = .center
        return label
    }()

    lazy var lblFailureIcon: UILabel = {
        let label = UILabel()
        label.text = "❌ 🥺"
        label.font = UIFont.systemFont(ofSize: 60)
        return label
    }()

    lazy var bttMainMenu: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Back to Main Menu", for: .normal)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.addSubview(self.lblTitle)
        self.addSubview(self.lblFailureIcon)
        self.addSubview(self.bttMainMenu)

        self.lblTitle.snp.remakeConstraints { make in
            make.top.equalTo(self.safeArea.top).offset(16)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.right).offset(-16)
        }

        self.lblFailureIcon.snp.remakeConstraints { make in
            make.center.equalTo(self.snp.center)
        }

        self.bttMainMenu.snp.remakeConstraints { make in
            make.top.equalTo(self.lblFailureIcon.snp.bottom).offset(16)
            make.centerX.equalTo(self.snp.centerX)
        }

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

