import Foundation
import UIKit

enum QuestionFailReson {
    case timeout
    case incorrectAnswer
}

protocol AnswerFailedVCDelegate {
    func didClickBttGoToMainMenu()
}

class AnswerFailedVC: UIViewController {

    private unowned var rootView: AnswerFailedVCView { return self.view as! AnswerFailedVCView }
    var coordinator: AnswerFailedVCDelegate?

    override func loadView() {
        self.view = AnswerFailedVCView()
        self.rootView.bttMainMenu.addTarget(
            self,
            action: #selector(bttGoToMainMenuClicked),
            for: .touchUpInside
        )
    }

    override func viewDidLoad() {
        self.navigationItem.title = "You failed :("
    }

    @objc func bttGoToMainMenuClicked() {
        self.coordinator?.didClickBttGoToMainMenu()
    }

    func setReason(reason: QuestionFailReson) {
        switch reason {
        case .timeout:
            self.rootView.lblTitle.text = "Time is up!"
            self.rootView.lblFailureIcon.text = "⏱🥺"
        case .incorrectAnswer:
            self.rootView.lblTitle.text = "Your Answer is incorrect!"
            self.rootView.lblFailureIcon.text = "❌🥺"
        }
    }
}
