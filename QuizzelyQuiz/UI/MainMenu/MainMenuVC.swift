import Foundation
import UIKit

protocol MainMenuVCDelegate {
    func didClickBttNewGame()
}

class MainMenuVC: UIViewController {

    private unowned var rootView: MainMenuVCView { return self.view as! MainMenuVCView }
    var coordinator: MainMenuVCDelegate?

    override func loadView() {
        self.view = MainMenuVCView()
    }

    override func viewDidLoad() {
        self.navigationItem.title = "QuizzelyQuiz"

        self.rootView.bttStartPlaying.addTarget(
            self,
            action: #selector(self.bttStartPlayingClicked),
            for: .touchUpInside
        )
    }


    override func viewDidAppear(_ animated: Bool) {
        self.rootView.lblHighscore.text = "🏆 Highscore: \(ScoreDAO.getHighscore())"
        self.rootView.lblOverallScore.text = "🏆 Overall Score: \(ScoreDAO.getOverallScore())"
    }

    @objc func bttStartPlayingClicked() {
        self.coordinator?.didClickBttNewGame()
    }

}
