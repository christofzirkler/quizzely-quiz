import Foundation
import UIKit
import SnapKit

class MainMenuVCView: UIView {

    lazy var bttStartPlaying: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 32  )
        button.setTitle("🎮 Start Playing 🎲", for: .normal)
        return button
    }()

    lazy var lblHighscore: UILabel = {
        let label = UILabel()
        label.text = ""
        return label
    }()

    lazy var lblOverallScore: UILabel = {
        let label = UILabel()
        label.text = "🏆 Overall Score: 777"
        return label
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.backgroundColor = .white

        self.addSubview(self.bttStartPlaying)
        self.addSubview(self.lblHighscore)
        self.addSubview(self.lblOverallScore)

        self.bttStartPlaying.snp.remakeConstraints { make in
            make.center.equalTo(self.snp.center)
        }

        self.lblHighscore.snp.remakeConstraints { make in
            make.top.equalTo(self.bttStartPlaying.snp.bottom).offset(24)
            make.centerX.equalTo(self.snp.centerX)
        }

        self.lblOverallScore.snp.remakeConstraints { make in
            make.top.equalTo(self.lblHighscore.snp.bottom).offset(16)
            make.centerX.equalTo(self.snp.centerX)
        }
    }
}
