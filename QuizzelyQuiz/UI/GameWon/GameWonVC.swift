import Foundation
import UIKit


protocol GameWonVCDelegate {
    func didClickBttGoToMainMenu()
}

class GameWonVC: UIViewController {

    private unowned var rootView: GameWonVCView { return self.view as! GameWonVCView }
    var coordinator: GameWonVCDelegate?

    override func loadView() {
        self.view = GameWonVCView()
    }

    override func viewDidLoad() {
        self.rootView.bttMainMenu.addTarget(
            self,
            action: #selector(bttDidClickGoToMainMenu),
            for: .touchUpInside
        )
    }

    @objc func bttDidClickGoToMainMenu() {
        self.coordinator?.didClickBttGoToMainMenu()
    }
}
