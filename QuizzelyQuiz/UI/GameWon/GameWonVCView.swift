import Foundation
import UIKit

class GameWonVCView: UIView {

    lazy var lblGameWon: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 35)
        label.text = "🎊 🎉 🏆 You Won!"
        return label
    }()

    lazy var bttMainMenu: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Go To Main Menu", for: .normal)
        return button
    }()

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.lblGameWon)
        self.addSubview(self.bttMainMenu)
        self.backgroundColor = .white

        self.lblGameWon.snp.remakeConstraints { make in
            make.center.equalTo(self.snp.center)
        }

        self.bttMainMenu.snp.remakeConstraints { make in
            make.top.equalTo(self.lblGameWon.snp.bottom).offset(16)
            make.centerX.equalTo(self.snp.centerX)
        }
    }
}
