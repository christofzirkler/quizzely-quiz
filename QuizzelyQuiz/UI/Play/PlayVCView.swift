import Foundation
import UIKit

class PlayVCView: UIView {

    lazy var lblTrophyIcon: UILabel = {
        let label = UILabel()
        label.text = "🏆"
        label.font = UIFont.systemFont(ofSize: 30)
        return label
    }()

    lazy var lblScoreCount: UILabel = {
        let label =  UILabel()
        label.text = "12345"
        return label
    }()

    lazy var lblTimeIcon: UILabel = {
        let label = UILabel()
        label.text = "⏱"
        label.font = UIFont.systemFont(ofSize: 30)
        return label
    }()

    lazy var lblTimeValue: UILabel = {
        let label = UILabel()
        label.text = "30s"
        return label
    }()

    lazy var seperator1: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()

    lazy var lblQuestionTitle: UILabel = {
        let label = UILabel()
        label.text = "<Question Title>"
        label.font = UIFont.systemFont(ofSize: 34)
        return label
    }()

    lazy var lblQuestion: UILabel = {
        let label = UILabel()
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr."
        return label
    }()


    lazy var txtUserGuess: UITextField  =  {
        let  txtUserGuess  = UITextField()
        txtUserGuess.placeholder = "Type your answer here  ..."
        txtUserGuess.borderStyle = .roundedRect
        return txtUserGuess
    }()

    lazy var bttCheck:  UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Check", for: .normal)
        return button
    }()

    lazy var bttHint:  UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Hint", for: .normal)
        return button
    }()

    lazy var seperator2: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.lblTrophyIcon)
        self.addSubview(self.lblScoreCount)
        self.addSubview(self.lblTimeIcon)
        self.addSubview(self.lblTimeValue)
        self.addSubview(self.seperator1)
        self.addSubview(self.lblQuestionTitle)
        self.addSubview(self.lblQuestion)
        self.addSubview(self.txtUserGuess)
        self.addSubview(self.bttCheck)
        self.addSubview(self.bttHint)
        self.addSubview(self.seperator2)
        self.backgroundColor = .white


        self.lblTrophyIcon.snp.remakeConstraints { make in
            make.left.equalTo(self.snp.left).offset(16)
            make.top.equalTo(self.safeArea.top).offset(16)
        }

        self.lblScoreCount.snp.remakeConstraints { make in
            make.left.equalTo(self.lblTrophyIcon.snp.right).offset(8)
            make.centerY.equalTo(self.lblTrophyIcon.snp.centerY)
        }

        self.lblTimeIcon.snp.remakeConstraints { make in
            make.right.equalTo(self.snp.right).offset(-16)
            make.top.equalTo(self.safeArea.top).offset(16)
        }

        self.lblTimeValue.snp.remakeConstraints { make in
            make.right.equalTo(self.lblTimeIcon.snp.left).offset(-8)
            make.centerY.equalTo(self.lblTimeIcon.snp.centerY)
        }

        self.seperator1.snp.remakeConstraints { make in
            make.top.equalTo(self.lblTrophyIcon.snp.bottom).offset(16)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.right).offset(-16)
            make.height.equalTo(1)
        }

        self.lblQuestionTitle.snp.remakeConstraints { make in
            make.top.equalTo(self.seperator1.snp.top).offset(16)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.right).offset(16)
        }

        self.lblQuestion.snp.remakeConstraints { make in
            make.top.equalTo(self.lblQuestionTitle.snp.bottom).offset(8)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.right).offset(-16)
        }

        self.txtUserGuess.snp.remakeConstraints { make in
            make.top.equalTo(self.lblQuestion.snp.bottom).offset(16)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.right).offset(-16)
        }

        self.bttHint.snp.remakeConstraints { make in
            make.top.equalTo(self.txtUserGuess.snp.bottom).offset(16)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.centerX)
        }

        self.bttCheck.snp.remakeConstraints { make in
            make.top.equalTo(self.txtUserGuess.snp.bottom).offset(16)
            make.left.equalTo(self.snp.centerX)
            make.right.equalTo(self.snp.right).offset(-16)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
