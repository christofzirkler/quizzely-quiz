import Foundation
import UIKit

protocol PlayVCDelegate {
    func didClickCheck(userAnswer: String)
    func didClickHint()
}

class PlayVC: UIViewController {

    private unowned var rootView: PlayVCView { return self.view as! PlayVCView }
    var coordinator: PlayVCDelegate?

    override func loadView() {
        self.view = PlayVCView()
    }

    override func viewDidLoad() {
        self.rootView.bttCheck.addTarget(self, action: #selector(bttCheckClicked), for: .touchUpInside)
        self.rootView.bttHint.addTarget(self, action: #selector(bttHintClicked), for: .touchUpInside)
    }

    func setupWithQuestion(question: QuizQuestion, questionIndex: Int, maxQuestions: Int) {
        self.rootView.txtUserGuess.text = ""
        self.navigationItem.title = "Question \(questionIndex + 1) / \(maxQuestions)"
        self.rootView.lblQuestionTitle.text = question.category.title
        self.rootView.lblQuestion.text = question.question
    }

    func setScore(score: Int) {
        self.rootView.lblScoreCount.text = "\(score)"
    }

    func setRemainingTime(seconds: Int) {
        self.rootView.lblTimeValue.text = "\(seconds)s"
    }

    func showHint(hint: String) {
        self.rootView.txtUserGuess.text = hint
    }

    @objc func bttCheckClicked()  {
        self.coordinator?.didClickCheck(userAnswer: self.rootView.txtUserGuess.text ?? "")
    }

    @objc func bttHintClicked()  {
        self.coordinator?.didClickHint()
    }

}


