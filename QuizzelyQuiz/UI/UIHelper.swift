import UIKit
import SnapKit

extension UIView {

    /**
     UIView extension which allows us to easily reference to the safeArea of the current screen.
     So we can make sure no views are hidden under the navigation bar or interfere with the iPhone X
     home inidicator on the bottom of the screen.
     */
    var safeArea: ConstraintBasicAttributesDSL {
        #if swift(>=3.2)
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.snp
        }
        return snp
        #else
        return snp
        #endif
    }
}

extension UINavigationController {
    /**
     Pushes a view controller to the stack and then removes all previous view controllers.
     Please notice that the clearing of the navigation stack is not performed synchronously.
     */
    func pushAndClearViewController(viewController: UIViewController) {
        self.setViewControllers([viewController], animated: true)
    }

    func replaceCurrentViewController(with viewController: UIViewController) {
        var navStack = self.viewControllers
        _ = navStack.popLast()
        navStack.append(viewController)
        self.setViewControllers(navStack, animated: true)
    }


}

