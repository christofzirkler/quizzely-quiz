import Foundation
import UIKit

protocol AnswerCorrectDelegate {
    func didClickNextQuestion()
}

class AnswerCorrectVC: UIViewController {

    private unowned var rootView: AnswerCorrectVCView { return self.view as! AnswerCorrectVCView }
    var coordinator: AnswerCorrectDelegate?

    override func loadView() {
        self.view = AnswerCorrectVCView()
    }

    override func viewDidLoad() {
        self.navigationItem.title = "Correct!"
        self.rootView.bttNextQuestion.addTarget(
            self,
            action: #selector(bttNextQuestionClicked),
            for: .touchUpInside
        )
    }

    @objc func bttNextQuestionClicked() {
        self.coordinator?.didClickNextQuestion()
    }
}
