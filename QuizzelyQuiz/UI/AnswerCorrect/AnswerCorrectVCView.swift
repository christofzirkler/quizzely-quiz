import Foundation
import UIKit

class AnswerCorrectVCView: UIView {

    lazy var lblTitle: UILabel = {
        let  label = UILabel()
        label.text = "Your Answer is correct!"
        label.textAlignment = .center
        return label
    }()

    lazy var lblSuccessIcon: UILabel = {
        let label = UILabel()
        label.text = "✅ 🥳"
        label.font = UIFont.systemFont(ofSize: 60)
        return label
    }()

    lazy var bttNextQuestion: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Next Question", for: .normal)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.addSubview(self.lblTitle)
        self.addSubview(self.lblSuccessIcon)
        self.addSubview(self.bttNextQuestion)

        self.lblTitle.snp.remakeConstraints { make in
            make.top.equalTo(self.safeArea.top).offset(16)
            make.left.equalTo(self.snp.left).offset(16)
            make.right.equalTo(self.snp.right).offset(-16)
        }

        self.lblSuccessIcon.snp.remakeConstraints { make in
            make.center.equalTo(self.snp.center)
        }

        self.bttNextQuestion.snp.remakeConstraints { make in
            make.top.equalTo(self.lblSuccessIcon.snp.bottom).offset(16)
            make.centerX.equalTo(self.snp.centerX)
        }

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

