import Foundation
import UIKit

class Colors {
    static let COLOR_PRIMARY = UIColor(red:0.08, green:0.23, blue:0.34, alpha:1.0)
    static let COLOR_PRIMARY_LIGHT = UIColor(red:0.59, green:0.73, blue:0.85, alpha:1.0)
}
