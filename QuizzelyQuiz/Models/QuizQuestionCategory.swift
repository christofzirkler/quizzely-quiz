import Foundation

class QuizQuestionCategory: Decodable {
    var id: Int
    var title: String

    enum CodingKeys: String, CodingKey {
        case id
        case title
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(Int.self, forKey: .id)
        self.title = try values.decode(String.self, forKey: .title)
    }

}
