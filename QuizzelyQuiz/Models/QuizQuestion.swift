import Foundation

class QuizQuestion: Decodable {

    var id: Int
    var question: String
    var answer: String
    var category: QuizQuestionCategory

    enum CodingKeys: String, CodingKey {
        case id
        case question
        case answer
        case category
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(Int.self, forKey: .id)
        self.question = try values.decode(String.self, forKey: .question)
        self.answer = try values.decode(String.self, forKey: .answer)
        self.category = try values.decode(QuizQuestionCategory.self, forKey: .category)
    }
}
